const express = require('express');
const app = express();
const morgan = require('morgan'); //to log the rotes
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')
const todoRoutes = require('./api/routes/todo');


mongoose.connect('mongodb://localhost/todo_list',{useNewUrlParser: true});

//to turn of deprecated warning of mongoose
mongoose.set('useCreateIndex', true);


app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
//app.use('*',cors());
app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept,Authorization");
    if (req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

app.use('/todo',todoRoutes);


 app.use((req, res, next) => {
     const error = new Error('Not found');
     error.status=404;
     next(error);
 });
////llll
 app.use((error,req,res,next)=>{
     res.status(error.status||500);
     res.json({
         error:{
             message:error.message
         }
     });
 });

module.exports = app;