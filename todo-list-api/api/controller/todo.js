const mongoose = require('mongoose');
const Todolist = require('../model/todolist');

exports.todo_get_all = (req, res, next) => {
    Todolist.find().select().exec().then( docs => {
        console.log(docs);
        // const response = {
        //     count: docs.length,
        //     todoList: docs.map(doc => {
        //         return {
        //             task: doc.task,
        //             state: doc.state,
        //         }
        //     })
        // };
        res.status(200).json(docs);
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

exports.todo_get_all_Completed = (req, res, next) => {
    Todolist.find({state: "done"}).select().exec().then( docs => {
        console.log(docs);
        // const response = {
        //     count: docs.length,
        //     todoList: docs.map(doc => {
        //         return {
        //             task: doc.task,
        //             state: doc.state,
        //         }
        //     })
        // };
        res.status(200).json(docs);
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

exports.todo_get_all_Pending = (req, res, next) => {
    Todolist.find({state: "pending"}).select().exec().then( docs => {
        console.log(docs);
        // const response = {
        //     count: docs.length,
        //     todoList: docs.map(doc => {
        //         return {
        //             task: doc.task,
        //             state: doc.state,
        //         }
        //     })
        // };
        
        res.status(200).json(docs);
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

exports.todo_create_todoList = (req, res, next) => {
    debugger;
    console.log(req.body);
    const td = new Todolist({
        _id: new mongoose.Types.ObjectId(),
        task:req.body.task,
        state: req.body.state
    });
    td.save()
    .then(result => {
        console.log(result);
        console.log("todoCreated");
        res.status(200).json({
            result:result
        });
    })
    .catch(err => console.log(err));
    //res.status(200).json({
   //     message:'post'
   // });

};

exports.todo_update_todoList = (req,res,next)=>{
    const id = req.params.todoId;
    console.log("Into update: "+id);
    // const updateOps = {};
    // for(const ops of req.body){
    //     updateOps[ops.propName] = ops.value;
    // }

    Todolist.update({_id:id},{$set:{state:"done"}})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            result:result,
            message: 'todo updated',
            url: 'http://localhost:3000/todo/'+id
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};