const mongoose = require('mongoose');


const todolistSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    task:{type:String, required:true},
    state:{type:String, required:true}
    
});

module.exports = mongoose.model('Todolist',todolistSchema);