const express = require('express');
const router = express.Router();
const Todolist = require('../model/todolist');
const mongoose = require('mongoose');
const todolistController = require('../controller/todo');

// router.get('/:todoId',(req,res,next)=>{
//     Todolist.find({_id : req.params.todoId}).select().exec().then( docs => {
//         console.log(docs);
//         const response = {
//             count: docs.length,
//             todoList: docs.map(doc => {
//                 return {
//                     task: doc.task,
//                     state: doc.state,
//                 }
//             })
//         };
//         res.status(200).json(response);
//     }).catch(err => {
//         console.log(err);
//         res.status(500).json({
//             error:err
//         });
//     });
// });

// router.post('/',(req,res,next)=>{
//     const td = new Todolist({
//         _id: new mongoose.Types.ObjectId(),
//         task:req.body.task,
//         state: req.body.state
//     });
//     td.save()
//     .then(result => {
//         console.log(result);
//     })
//     .catch(err => console.log(err));
//     res.status(200).json({
//         message:'post'
//     });
// });



// router.patch('/:todoId',(req,res,next)=>{
//     const id = req.params.todoId;
//     console.log(id);
//     res.status(200).json({
//         message:'updated'
//     });
// });

// router.delete('/:todoId',(req,res,next)=>{
//     const id = req.params.todoId;
//     res.status(200).json({
//         message:'deleted'
//     });
// });



router.get('/',  todolistController.todo_get_all);
router.get('/completed',  todolistController.todo_get_all_Completed);
router.get('/pending',  todolistController.todo_get_all_Pending);

router.post('/',  todolistController.todo_create_todoList);

// router.get('/:orderId', todolistController.todo_get_todoList);

// router.delete('/:orderId', todolistController.todo_delete_todoList);

router.patch('/:todoId', todolistController.todo_update_todoList);


module.exports = router;