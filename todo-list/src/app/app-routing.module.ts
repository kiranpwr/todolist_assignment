import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewTodoComponent } from './new-todo/new-todo.component';
import { CompletedTodoComponent } from './completed-todo/completed-todo.component';
import { PendingTodoComponent } from './pending-todo/pending-todo.component';

const routes: Routes = [
  {path: '', redirectTo: '/todo', pathMatch: 'full'},
  {path:'todo',component:NewTodoComponent},
  {path:'pendingTodo',component:PendingTodoComponent},
  {path:'completeTodo',component:CompletedTodoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
