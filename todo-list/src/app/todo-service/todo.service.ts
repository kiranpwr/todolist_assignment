import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  Observable, of} from 'rxjs';
import { ITodo } from '../model/todo';
import { catchError, map, tap } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
 };

@Injectable({
  providedIn: 'root',

})
export class TodoService {
 

  private _Url = "http://localhost:3000/todo";
  private _Urlcom = "http://localhost:3000/todo/completed";
  private _Urlpen = "http://localhost:3000/todo/pending";

  constructor(private http : HttpClient) { }

  getTodo():Observable<ITodo[]>{
    debugger;
    console.log('todoServices :fetched');
    //return of(HEROES);
    return this.http.get<ITodo[]>(this._Url)
    .pipe(
      tap(_ => console.log('fetched heroes')),
      catchError(this.handleError<ITodo[]>('getTodo', []))
    );
  };

  getCompleteTodo():Observable<ITodo[]>{
    debugger;
    console.log('todoServices complete:fetched');
    //return of(HEROES);
    return this.http.get<ITodo[]>(this._Urlcom)
    .pipe(
      tap(_ => console.log('fetched completed heroes')),
      catchError(this.handleError<ITodo[]>('getCompleteTodo', []))
    );
  };

  getPendingTodo():Observable<ITodo[]>{
    debugger;
    console.log('todoServices :fetched');
    //return of(HEROES);
    return this.http.get<ITodo[]>(this._Urlpen)
    .pipe(
      tap(_ => console.log('fetched pending heroes')),
      catchError(this.handleError<ITodo[]>('getPendingTodo', []))
    );
  };

  addTask(todo:ITodo){
    debugger;
    return this.http.post<ITodo>(this._Url, todo)
    .pipe( 
      tap((newTodo:ITodo) => console.log(`added todo w/ id=${newTodo}`)),
      catchError(this.handleError<ITodo>('addTask'))
    );
  }

  _state = "done";

  updateTodo(_id):Observable<ITodo> {
    debugger;
    
      return this.http.patch(this._Url+'/'+_id,httpOptions).pipe(
        tap(_ => console.log(`updated todo id=${_id}`)),
        catchError(this.handleError<any>('updateTodo'))
      );
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
