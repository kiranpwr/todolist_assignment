import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo-service/todo.service';
import { ITodo } from '../model/todo';

@Component({
  selector: 'app-completed-todo',
  templateUrl: './completed-todo.component.html',
  styleUrls: ['./completed-todo.component.css']
})
export class CompletedTodoComponent implements OnInit {
  todoTask :ITodo[];
  constructor(private _todoServices:TodoService) { }

  ngOnInit() {
    this.getCompleteTasks();
  }

  getCompleteTasks(){
    debugger;
    this._todoServices.getCompleteTodo()
     .subscribe(todos => {
      console.log(todos);
      this.todoTask = todos});
     console.log(this.todoTask);
  }

  changeStatus(id){
    debugger;
      console.log("_id"+id);
      this._todoServices.updateTodo(id).subscribe();
  };
}
