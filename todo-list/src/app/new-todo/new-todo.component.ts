import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ITodo } from '../model/todo';
import { TodoService } from '../todo-service/todo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.css']
})
export class NewTodoComponent implements OnInit {
   closeResult: string;
   //countries =[{name:'india', area:400, population:500}];


    todoTask : ITodo[] ;
    rstd = [];
   todoModel = new ITodo("","pending");
   page = 1;
   pageSize = 4;
   collectionSize = this.rstd.length;
 
   get todoTaskPages(): ITodo[] {
     debugger;
     return this.todoTask
       .map((todo, i) => ({id: i + 1, ...todo}))
       .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
   }
    
   constructor(private modalService: NgbModal, private _todoServices : TodoService) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

 

  private getDismissReason(reason: any): string {
    debugger;
    if ( reason === ModalDismissReasons.ESC ) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  };


  ngOnInit() {
    this.getAllTasks();
  };

  getAllTasks(){
    debugger;
    this._todoServices.getTodo().subscribe(todos => 
      {
        console.log(todos);
        this.todoTask = todos;
        this.rstd=todos
      });
        console.log(this.todoTask);
  };

  onSubmit(){
    debugger;
    
    console.log(this.todoModel);
    this._todoServices.addTask(this.todoModel).subscribe(todo => {console.log('Success!',todo); this.getAllTasks();console.log(this.todoTask)},
                                                         error => console.error('Error!',error)
    );
      debugger;
     
  };

  changeStatus(id){
    debugger;
      console.log("_id"+id);
      this._todoServices.updateTodo(id).subscribe(uptodo =>{console.log('Update Success!',uptodo);this.getAllTasks()},
                                                  error => console.error('Error!',error)                    
                                                    );
  };

  
}
