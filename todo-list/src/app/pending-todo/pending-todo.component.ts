import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo-service/todo.service';
import { ITodo } from '../model/todo';

@Component({
  selector: 'app-pending-todo',
  templateUrl: './pending-todo.component.html',
  styleUrls: ['./pending-todo.component.css']
})
export class PendingTodoComponent implements OnInit {
  
   todoTask :ITodo[];;
  constructor(private _todoServices:TodoService) { }

  ngOnInit() {
    this.getPendingTasks();
  }

  getPendingTasks(){
    debugger;
    this._todoServices.getPendingTodo()
     .subscribe(todos => {
      console.log(todos);
      this.todoTask = todos});
     console.log(this.todoTask);
  }

  changeStatus(id){
    debugger;
      console.log("_id"+id);
      this._todoServices.updateTodo(id).subscribe(uptodo =>{console.log('Update Success!',uptodo);this.getPendingTasks()},
                                                  error => console.error('Error!',error)                    
                                                    );
  };
}
